# nodejs

NodeJS implementation of JustBytes

<br>

## Table of Contents

* [Why this package? How to use it?](#why-this-package--how-to-use-it)
* [Specs](#specs)
* [Installation](#installation)
* [How to?](#how-to)
* [Functions](#functions)
  * [saveMultipartFile](#savemultipartfile)
* [Complete Workflow](#complete-workflow)
* [CI/CD](cd-cd)

<br>

## Why this package? How to use it?

This package was built to receive (sending is in development) files; these files arrive as chunks, where each chunk is a base 64 encoded string of raw bytes. It is possible to receive a file is one piece - provided it is a base 64 encoded string.

This package was also built to play nicely with [this](https://gitlab.com/treuzedev/justbytes/js) Javascript module. It is not necessary though, as long as the requirements are met, you can use your own implementation.

<br>

The high-level explanation (see [this](#complete-workflow) simple coding example for a better understanding):
* A chunk is received
* The function saves that chunk in a temporary folder
* Then it evaluates if all chunks have been received (if a file has only one chunk, ie, a whole file is being received, nothing else needs to be done, as the package assumes all chunks have been received)
* If they have, then all chunks are pieced together, and the file is saved in a specified directory

<br>

## Specs

Current deployed versions in NPM:
* `0.1.0`

<br>

NodeJS tested versions:
* `v10.23.2`

<br>

File extensions tested:
* gif, jpg, png
* json, pdf, txt

<br>

## Installation

See the [NPM](https://www.npmjs.com/package/@treuzedev/justbytes) page.

Or, simply, `npm i @treuzedev/justbytes`.

<br>

## How to

```
// simple server, not production ready
const express = require('express');
const cors = require('cors');

// in addition to the justbytes package, a parser may be needed to correctly transform the body into a JSON object
const bodyParser = require('body-parser');
const justbytes = require('justbytes');

const app = express();
const port = 80;

app.use(cors());
app.use(bodyParser.json({'limit': '50mb'}));

// a route to send the chunks
app.post('/save-file', (request, response) => {
    const status = justbytes.saveMultipartFile(request.body);
    response.send(status);
});

app.listen(port, () => {
    console.log(`Test app listening on https://subdomain.domain.com`);
});
```

<br>

## Functions

<br>

### saveMultipartFile

Exactly one argument needs to be passed to the function, an object (described below).

That object keys are also locked.

Returns an object indicating if the chunk was saved successfully, and, when the final chunk is received, return an object indicating if the file was saved successfully or not.

```
// if the body does not adhere to this format, it is rejected
const body = {
    'tmpFolderName': '/path/to/dir/where/chunks/are/saved',
    'filename': 'filename',
    'extension': 'extension',
    'finalDestination': '/path/to/where/file/is/saved',
    'numberOfChunks': 13,
    'chunk': 1,
    'data': 'aBase64EncodedStringOfRawBytes',
};


//
const status = saveMultipartFile(body);

// when a chunk is saved
status = {
    'status': 200,
    'message': 'chunk saved successfully!',
    'chunk': 51, // chunk position
    'filename': 'nameOfTheFileBeingSaved.extension',
}

// when a file is created
status = {
    'status': 200,
    'message': 'file created successfully!',
    'path': '/path/to/saved/file.extension',
}

// when an error occurs
status = {
    'status': 500,
    'message': 'a very informative error message',
}
```

<br>

#### Body fields

<br>

`body['tmpFolderName']:`

A `string` indicating where the chunks are saved.

<br>

`body['filename']:`

A `string` indicating the filename of the future whole file.

<br>

`body['extension']:`

A `string` indicating the extension of the future whole file; can be an empty string if file has no extension. Currently only files with one extension are supported, ie, files like `.tar.gz` may not be saved properly.

<br>

`body['finalDestination']:`

A `string` indicating where the future whole file is saved. If `body['filename'] = 'test'`, `body['extension'] = 'png'` and `body['finalDestination'] = '/home/user'`, then the file is saved over at `/home/user/test.png`.

<br>

`body['numberOfChunks']:`

The number of chunks the original file was divided into. Must be an `integer`.

<br>

`body['chunk']:`

The position of the chunk. Must be an `integer`.

<br>

`body['data']:`

The base 64 encoded `string` of raw bytes corresponding to a part of the file.

<br>

## Complete Workflow

This workflow assumes the use of the justbytes package for the client side ([here](https://gitlab.com/treuzedev/justbytes/js/-/tree/master)).

Also assumes that this is for demonstration purposes and not production ready.

#### Client (Browser)

```
<html>

  <head>
    <title>JustBytes Testing</title>
  </head>

  <body>

    <!-- lets a user select a file -->
    <input id="input" type="file" />

    <!-- JS -->
    <script type="text/javascript" src="https://cdn.treuze.dev/js/main.js"></script>
    <script>
        
        <!-- a file is sent after the user selects one -->
        const endpoint = 'https://subdomain.domain.com/save-file';
        sendFileFromTag(document.getElementById('input'), endpoint);
        
    </script>

  </body>

</html>
```

<br>

#### Server (NodeJS)

```
// simple server, not production ready
const express = require('express');
const cors = require('cors');

// in addition to the justbytes package, a parser may be needed to correctly transform the body into a JSON object
const bodyParser = require('body-parser');
const justbytes = require('justbytes');

const app = express();
const port = 80;

app.use(cors());
app.use(bodyParser.json({'limit': '50mb'}));

// a route to send the chunks
// when all chunks have arrived, the file is saved to request.body.finalDestination
app.post('/save-file', (request, response) => {
    const status = justbytes.saveMultipartFile(request.body);
    response.send(status);
});

app.listen(port, () => {
    console.log(`Test app listening on https://subdomain.domain.com`);
});
```

<br>

## CI/CD

CI/CD information can be found [here](https://gitlab.com/treuzedev/justbytes/cicd#nodejs).
