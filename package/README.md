# justbytes

A package to receive files (sending is in development) as multipart strings.

See the [README.md](https://gitlab.com/treuzedev/justbytes/nodejs) over at Gitlab for more information on how to work with this package.
